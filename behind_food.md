---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2021/2022
titre: Behind food - Amélioration d’une application mobile sur la face cachée des aliments industriels
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - Samuel Fringeli 
professeurs co-superviseurs:
  - Non Attribué
mots-clé: [Food,Application Mobile]
langue: [F,E]
confidentialité: non
suite: non
---
\begin{center}
\includegraphics[width=0.3\textwidth]{images/smartphone.png}
\end{center}

## Description
Behind food est une application mobile utilisée dans le cadre d’une exposition sur le développement durable. Elle permet aux visiteurs d’explorer les faces cachées de différents aliments du quotidien au moyen d’un parcours de divers thèmes reliés à ces aliments. Ce parcours donne accès à des images, à des vidéos et à des textes pour illustrer les caractéristiques des aliments concernés. Ces éléments sont mis à jour par l’équipe qui gère l’exposition, au moyen d’une interface backend, et sont accessibles depuis l’application grâce à une API.
Dans la version actuelle de l’application, c’est une webview qui est chargée et qui affiche les images et vidéos au fur et à mesure du parcours de l’utilisateur dans la structure, mais les médias ne sont pas sauvegardés dans le stockage local de l’application, ce qui rend impossible l’utilisation de celle-ci hors ligne. Comme l’exposition a pour objectif de fonctionner entièrement hors ligne, il serait nécessaire de faire en sorte que les données affichées soient téléchargées localement, avec un système permettant d’actualiser les dernières modifications effectuées sur le backend par l’utilisateur de l’application.

## Buts
Ce projet a pour but d’analyser les différentes possibilités pour rendre l’application disponible sans connexion internet et de réaliser un prototype remplissant cet objectif.

## Objectifs/Tâches

- Elaboration d'un cahier des charges
- Analyse de l'applications existance et d'autres, ainsi des technologies possibles pour cette applications au niveua des plateformes de développement (Android, iOS, Xamarin, Flutter)
- Conception d'un POC (Proof of concept)
- Implementation du POC (application mobile)
- Tests et validation du système.
- Rédaction d'une documentation adéquate.

